# ssh-login-msg.sh

with ssh-login-msg.sh you can track Login´s at your Server and Send it to Gotify.

## Recommended

+ [curl](https://github.com/curl/curl)
+ [Gotify Server](https://gotify.net/)

---

### How it works

#### Autoinstall:

```
bash <(curl -s https://gitlab.com/philw95/public/-/raw/main/ssh-login-msg/install.sh)
```

#### Manual installation:

First download this File:

1. Download the script `curl -O https://gitlab.com/philw95/public/-/raw/main/ssh-login-msg/ssh-login-msg.sh`
2. Set Executable flag `chmod +x ssh-login-msg.sh`
3. Copy to /etc/profile.d/ `cp ssh-login-msg.sh /etc/profile.d/`
4. Download or Create .env file `curl -O https://gitlab.com/philw95/public/-/raw/main/ssh-login-msg/.env` or `touch /.ssh-login-msg.sh.env`
    
    4.1 if you Download the file move it to the Root folder `cp .env /.ssh-login-msg.sh.env`   
5. Edit credencials `nano /ssh-login-msg.sh.env`

    5.1 we need:
        
    + `gotify_url="https://<gotify.example.com>/message?token="`
    + `gotify_token="<Your Token>"`

    5.2 optional:

    + Change day and time format `TODAY=$(date +'%a %d.%m.%Y %H:%M:%S')`
    + Change Priority (1-10) `PRIORITY="7"`

## Usage

A push notification is now sent with every login.

![Login Message](image-1.png)

#### You can test it with: `/etc/profile.d/ssh-login-msg.sh test`
![Test Message](image-2.png)


If you will have a Nortification at Server Boot you can add this in your crontab:

1. `crontab -e` (on first time you must select an editor)
2. add this at the bottom: `@reboot /etc/profile.d/ssh-login-msg.sh boot > /dev/null`

And now at Every Boot you become a Nortification.

![Start Message](image.png)