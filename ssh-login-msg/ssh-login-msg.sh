#!/usr/bin/env bash
#
# Version V1.1
#
# Created by: philw95

FILENAME="${BASH_SOURCE[0]##*/}"
   TODAY=$(date +'%a %d.%m.%Y %H:%M:%S')
 LOGFILE=~/"${FILENAME}.log"
PRIORITY="7"

_error() { echo "Error: Gotify - ${1}" && return ${2}; }

if [[ -f /".${FILENAME}.env" ]]; then
	source /".${FILENAME}.env"
	if [[ -n "${gotify_token}" ]]; then
		gotify_url="${gotify_url}${gotify_token}"
	else
		_error "No token" 2
	fi
else
	_error "No env file" 1
fi

if [[ "$1" == "test" ]]; then
	title="TestMessage @ ${HOSTNAME}"
	  msg=$(cat <<-EOF
			📢 This are a TESTMESSAGE ✅
			🕑 ${TODAY}
			EOF
			)
elif [[ "$1" == "boot" ]]; then
	title="Server Start @ ${HOSTNAME}"
	  msg=$(cat <<-EOF
			📢 Server was started ✅
			🕑 ${TODAY}
			EOF
			)
elif [[ -n "${SSH_CLIENT}" ]]; then
	IP_SERVER=$(hostname -I	| awk '{print $1}')
	  IP_USER=$(echo "${SSH_CLIENT}" | awk '{print $1}')
	    title="SSH Login @ ${HOSTNAME}"
		  msg=$(cat <<-EOF
				📢 SSH Login ${USER}@${HOSTNAME}
				🕑 ${TODAY}
				📡 Host: ${IP_SERVER} From: ${IP_USER}
				EOF
				)	
else
	title="Server Login @ ${HOSTNAME}"
	  msg=$(cat <<-EOF
			📢 Server Login ${USER}@${HOSTNAME}
			🕑 ${TODAY}
			EOF
			)
fi

send_gotify() {
	echo $msg >> "${LOGFILE}"
	curl -s --connect-timeout 15 -F "title=${title}" -F "message=${msg}" -F "priority=${PRIORITY}" "${gotify_url}" >/dev/null
}

send_gotify &
