#!/bin/bash

# Description:
# Automatic download script files

# Links
PROJECT_SIDE="https://gitlab.com/philw95/public"
      SCRIPT="ssh-login-msg"
 SCRIPT_NAME="${SCRIPT}.sh"
   SCRIPT_DL="${PROJECT_SIDE}/-/raw/main/ssh-login-msg/${SCRIPT_NAME}"
      ENV_DL="${PROJECT_SIDE}/-/raw/main/ssh-login-msg/.env"

# Define colors for printf
MAIN='\033[0m'
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'

# Special characters
GOOD="[${GREEN}✓${MAIN}]"
ERROR="[${RED}✗${MAIN}]"
NEUTRAL="[${YELLOW}-${MAIN}]"
INFO="[i]"

# Banner
clear
printf '#                                                                            #\n'
printf '#               __          __            _                                  #\n'
printf '#    __________/ /_        / /___  ____ _(_)___        ____ ___  _________ _ #\n'
printf '#   / ___/ ___/ __ \______/ / __ \/ __ `/ / __ \______/ __ `__ \/ ___/ __ `/ #\n'
printf '#  (__  |__  ) / / /_____/ / /_/ / /_/ / / / / /_____/ / / / / (__  ) /_/ /  #\n'
printf '# /____/____/_/ /_/     /_/\____/\__, /_/_/ /_/     /_/ /_/ /_/____/\__, /   #\n'
printf '#                               /____/                             /____/    #\n'
printf '#                                                                            #\n'

echo
if [ "$(id -u)" = "0" ]; then
        printf " ${GOOD} Root User \n"
		echo
else
        printf " ${ERROR} Not Root User! You must have Root Rights! Use 'su' or 'sudo -i' \n"
        exit 1
fi

printf " ${INFO} Download script file\n"
curl -sO ${SCRIPT_DL}
chmod +x ${SCRIPT_NAME}
mv ${SCRIPT_NAME} /etc/profile.d/
if [[ -f "/etc/profile.d/${SCRIPT_NAME}" ]]
then printf " ${GOOD} Download script file\n"
else printf " ${ERROR} Download script file\n"; exit 1
fi

read -n 1 -s -r -p " [Press any key to continue]"
echo

echo
read -e -i "https://<gotify.example.com>" -p " Your Gotify Server Adress: " gotify_url
read -e -p " Your App Token: " gotify_token
gotify_url="${gotify_url}/message?token="

echo
printf " ${NEUTRAL}   gotify_url: ${gotify_url}\n"
printf " ${NEUTRAL} gotify_token: ${gotify_token}\n"
echo
printf " ${INFO} If you will change this, edit the file /.${SCRIPT_NAME}.env\n"

# create .env file
echo "# Your Gotify URL" > /.${SCRIPT_NAME}.env
echo "gotify_url=\"${gotify_url}\"" >> /.${SCRIPT_NAME}.env
echo "# Your App Token" >> /.${SCRIPT_NAME}.env
echo "gotify_token=\"${gotify_token}\"" >> /.${SCRIPT_NAME}.env
echo "" >> /.${SCRIPT_NAME}.env
echo "" >> /.${SCRIPT_NAME}.env
echo "# optional" >> /.${SCRIPT_NAME}.env
echo "" >> /.${SCRIPT_NAME}.env
echo "# Change day and time format" >> /.${SCRIPT_NAME}.env
echo "#TODAY=\$(date +'%a %d.%m.%Y %H:%M:%S')" >> /.${SCRIPT_NAME}.env
echo "" >> /.${SCRIPT_NAME}.env
echo "# Change Priority (1-10)" >> /.${SCRIPT_NAME}.env
echo "#PRIORITY="7"" >> /.${SCRIPT_NAME}.env
echo "" >> /.${SCRIPT_NAME}.env

echo
read -n 1 -s -r -p " [Press any key to continue]"

echo
/etc/profile.d/${SCRIPT_NAME} test
printf " ${INFO} A test message was sent. Please check Gotify.\n"
echo

exit 0